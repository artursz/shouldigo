from scipy.interpolate import interp1d
import numpy as np
import math


def plot_fun(f, plot_range):
    import matplotlib.pyplot as plt
    xnew = np.linspace(plot_range[0], plot_range[1], num=1000, endpoint=True)
    plt.plot(xnew, f(xnew), '-')
    plt.show()


def plot_fun2d(fun, x_range, y_range):
    fun = np.vectorize(fun)
    x = np.linspace(*x_range, endpoint=True)
    y = np.linspace(*y_range, endpoint=True)
    # print('X', x)
    # print('Y', y)
    X, Y = np.meshgrid(x, y)
    from matplotlib.pyplot import colorbar, pcolor, show
    Z = fun(X, Y)
    # Z = np.array([[fun(xi, yi) for yi in y] for xi in x])
    # print(Z)
    pcolor(X, Y, Z)
    colorbar()
    show()


def make_score_function(data_tsv, plot=False, limit_range=None, valid_range=None):
    values = [r.strip().split() for r in data_tsv.split('\n') if r.strip()]
    x = np.array([float(v[0]) for v in values])
    y = np.array([float(v[1]) for v in values])
    f = interp1d(x, y)

    def score_fun(x):
        if type(x) is np.ndarray:
            return np.array([score_fun(v) for v in x])
        # print(x)
        if x is None or math.isnan(x): return -1
        if valid_range and (x < valid_range[0] or x > valid_range[1]): return -2
        if limit_range and (x < limit_range[0] or x > limit_range[1]): return -1
        return f(x)

    if plot:
        import matplotlib.pyplot as plt
        xnew = np.linspace(limit_range[0], limit_range[1], num=1000, endpoint=True)
        plt.plot(x, y, 'o', xnew, f(xnew), '-')
        plt.show()
    return score_fun


def score_multi(weights, scores, names=None, detailed=False):
    # print(weights, scores)
    weight_sum = 0
    weight_sum_valid = 0
    score_sum = 0

    for score, weight in zip(scores, weights):
        # print('score, weight', score, weight)
        if score >= 0:
            weight_sum_valid += weight
            score_sum += weight * score
        if score < -1:
            score_sum = 0
            break

    score = (score_sum / weight_sum_valid) if weight_sum_valid > 0 else 0

    if detailed:
        return dict(
            score=score,
            **{f: round(float(v), 6) for f, v in zip(names, scores)}
        )
    else:
        return score


score_ski_temp = make_score_function("""
-200 0
-50	0
-25	0
-6	1
-1	0
0	0
10	0
200 0
""", limit_range=[-100, 100], valid_range=[-100, 1], plot=False)


score_ski_snow = make_score_function("""
-1 0
0 0
.05 0
0.25 1
1 1
100 1
""", limit_range=[0, 100], valid_range=[0, 1000], plot=False)


def score_skiing(temp, snow, detailed=False):
    return score_multi(
        [1, 1],
        [score_ski_temp(temp), score_ski_snow(snow)],
        ['temp', 'snow'],
        detailed=detailed
    )


score_hiking_temp = make_score_function("""
-100 0
-10	0
10 0.2
20 1
30 0.2
35 0
100 0
""", limit_range=[-100, 100], valid_range=[-20, 40], plot=False)


score_hiking_precipitation = make_score_function("""
0 1
1 0.8
3 0.2
10 0
100 0
""", limit_range=[0, 100], valid_range=[-100, 10], plot=False)


score_hiking_wind = make_score_function("""
0 1
2 0.8
4 0.2
10 0
100 0
""", limit_range=[0, 100], valid_range=[0, 10], plot=False)


def score_hiking(temp, prec, wind, detailed=False):
    return score_multi(
        [2, 1, 1],
        [score_hiking_temp(temp), score_hiking_precipitation(prec), score_hiking_wind(wind)],
        ['temp', 'prec', 'wind'],
        detailed=detailed
    )

def score_hiking0(temp, prec, detailed=False):
    return score_multi(
        [2, 1],
        [ score_hiking_temp(temp), score_hiking_precipitation(prec)],
        ['temp', 'prec'],
        detailed=detailed
    )

if __name__ == '__main__':
    pass
    # plot_fun(score_ski_temp, [-200, 200])
    # plot_fun(score_ski_snow, [-5, 5])

    # plot_fun2d(score_skiing, [-30, 20, 50], [-1, 1, 20])
    # plot_fun2d(score_hiking, [-30, 20, 10], [-30, 40, 10])
    # plot_fun2d(score_hiking0, [10, 30, 20], [-3, 10, 10])
    # plot_fun2d(lambda temp, prec: score_hiking(temp, prec, 0), [-20, 40, 80], [-3, 20, 40])
    # plot_fun2d(lambda temp, prec: score_hiking(temp, prec, 1), [-20, 40, 80], [-3, 20, 40])
    # plot_fun2d(lambda temp, prec: score_hiking(temp, prec, 10), [-20, 40, 80], [-3, 20, 40])

    # print(score_hiking(10, 4, 0, detailed=True))
