import pygrib
import numpy as np
from datetime import datetime, timedelta, date
import os, sys
import json
import logging
import math
from copy import deepcopy
import gzip

import score

logger = logging.getLogger('gfs')
logging.basicConfig(format='%(asctime)s : %(name)s : %(levelname)s : %(message)s', level=logging.INFO)

# lat - y (-90,90), lon - x (-180,180)

def transform(grb_path):
    grbs = pygrib.open(grb_path)
    lats, lons = None, None
    shape = None
    for grb in grbs[:1]:
        # print(np.shape(grb.latlons()))  # (2, 361, 720)

        lats_raw, lons_raw = grb.latlons()
        # for r in lats_raw:
        #     for i in range(1, len(r)-1):
        #         assert r[i] == r[i-1]
        # for r in lons_raw.T:
        #     for i in range(1, len(r)-1):
        #         assert r[i] == r[i-1]
        lats = np.array(lats_raw)[:, 0]
        lons = np.array(lons_raw)[0, :]
        lons[lons > 180] -= 360
        shape = (len(lats), len(lons))

    values_wind = None
    values_prec = None
    values_temp = None
    values_snow = None
    values_snow_eq = None

    def get_values(grb):
        values = grb.values
        assert values.shape == shape
        return values

    for grb in grbs:
        if grb.parameterName == 'Wind speed [gust]':
            values_wind = get_values(grb)
        elif grb.parameterName == 'Total precipitation':
            values_prec = get_values(grb)
        elif grb.parameterName == 'Maximum temperature':
            values_temp = get_values(grb) - 273.15
        elif grb.parameterName == 'Snow depth':
            values_snow = get_values(grb)
        elif grb.parameterName == 'Water equivalent of accumulated snow depth':
            values_snow_eq = get_values(grb)

    assert values_wind is not None
    assert values_prec is not None
    assert values_temp is not None
    assert values_snow is not None
    assert values_snow_eq is not None

    return {
        'lats': lats,
        'lons': lons,
        'wind': values_wind,
        'prec': values_prec,
        'temp': values_temp,
        'snow': values_snow,
        'snow_eq': values_snow_eq,
    }


def date_range(start_date, end_date):
    for n in range(int((end_date - start_date).days + 1)):
        yield start_date + timedelta(n)


def get_points_of_interest(fpaths, dump_json=None):
    points = []
    for fpath in fpaths:
        data = json.load(open(fpath))
        activity = os.path.splitext(os.path.basename(fpath))[0].split('-', 1)[0]
        for p in data['features']:
            x, y = p['geometry']['coordinates']
            points.append({
                'id': p['id'],
                'x': x,
                'y': y,
                'activities': [activity]
            })
    if dump_json:
        with open(dump_json, 'w') as f:
            f.write(json.dumps(points, indent=2))


def read_json(fpath):
    if fpath.endswith('.gz'):
        with gzip.GzipFile(fpath, 'rb') as f:
            json.loads(f.read().decode('utf-8'))
    else:
        with open(fpath) as f:
             return json.load(f)


def write_json(data, fpath):
    if fpath:
        if fpath.endswith('.gz'):
            with gzip.open(fpath, 'wb') as f:
                f.write(json.dumps(data, indent=2).encode('utf-8'))

        os.makedirs(os.path.dirname(fpath), exist_ok=True)
        with open(fpath, 'w') as f:
             f.write(json.dumps(data, indent=2))


def featurize(wd, points, dump_json=None):
    def format_value(v):
        if math.isnan(v):
            return None
        return round(float(v), 6)

    lats, lons = wd['lats'], wd['lons']
    for p in points:
        lat_idx = (np.abs(lats - p['y'])).argmin()
        lon_idx = (np.abs(lons - p['x'])).argmin()
        if not(abs(lats[lat_idx] - p['y']) < 1 and abs(lons[lon_idx] - p['x']) < 1):
            print('point outside grid {}'.format([lats[lat_idx], lons[lon_idx], p]))

        p['measurements'] = {}
        wind = wd['wind'][lat_idx][lon_idx]
        p['measurements']['wind'] = format_value(wind)

        prec = wd['prec'][lat_idx][lon_idx]
        p['measurements']['prec'] = format_value(prec)

        temp = wd['temp'][lat_idx][lon_idx]
        p['measurements']['temp'] = format_value(temp)

        snow = wd['snow'][lat_idx][lon_idx]
        p['measurements']['snow'] = format_value(snow)

        snow_eq = wd['snow_eq'][lat_idx][lon_idx]
        p['measurements']['snow_eq'] = format_value(snow_eq)

        if 'hiking' in p['activities']:
            p['hiking'] = score.score_hiking(temp, prec, wind, detailed=True)
        if 'skiing' in p['activities']:
            p['skiing'] = score.score_skiing(temp, snow, detailed=True)

    write_json(points, dump_json)


def process_points(points_file, start_date, end_date, data_dir, out_dir):
    points = read_json(points_file)
    prev_existing_d = None
    os.makedirs(out_dir, exist_ok=True)
    for d in date_range(start_date, end_date):
        out_d = d.strftime('%m%d')
        in_d = d.strftime('%Y%m%d')
        logger.info('.. {}'.format(d))
        f = '{}/{}.grb2'.format(data_dir, in_d)
        if not os.path.exists(f):
            print('does not exist {}'.format(f))
            # continue
            in_d = prev_existing_d
            f = '{}/{}.grb2'.format(data_dir, in_d)
        else:
            prev_existing_d = in_d
        data = transform(f)
        featurize(data, points, dump_json='{}/{}.json'.format(out_dir, out_d))
    logger.info('finished')


def reverse_point_index(point_dir, files=None, dump_json=None):
    date_data = []
    if not files:
        files = os.listdir(point_dir)
    for f in sorted(files):
        if not f.endswith('.json'): continue
        data = read_json(os.path.join(point_dir, f))
        date_str = os.path.splitext(f)[0]
        date_data.append([date_str, data])

    points = deepcopy(date_data[0][1])

    for ip, p in enumerate(points):
        measurements = []
        for dd, data in date_data:
            data[ip]['measurements']['date'] = dd
            measurements.append(data[ip]['measurements'])
        p['measurements'] = measurements
        for activity in p['activities']:
            history = []
            for dd, data in date_data:
                data[ip][activity]['date'] = dd
                history.append(data[ip][activity])
            p[activity] = history

    if dump_json:
        write_json(points, dump_json)


def add_rev_index_best_intervals(rev_idx_path, out_path, min_avg_score, minimal=False):
    points = read_json(rev_idx_path)
    for p in points:
        if minimal: del p['measurements']
        for activity in p['activities']:
            best_interval = None
            best_interval_dates = None
            best_avg = None
            best_len = 0

            activity_arr = p[activity] + p[activity]
            for i, pi in enumerate(activity_arr):
                sum = 0
                for k in range(i, len(activity_arr)):
                    pk = activity_arr[k]
                    sum += pk['score']
                    avg = sum / (k - i + 1)
                    if (avg > min_avg_score and k-i+1 > best_len and k-i+1 <= len(p[activity])) or (best_len == 0) or (best_len == 1 and best_avg < avg):
                        best_len = k-i+1
                        best_interval = [i, k]
                        best_interval_dates = [pi['date'], pk['date']]
                        best_avg = avg

            if minimal: del p[activity]
            p[activity + '_best'] = dict(
                avg=best_avg,
                start=best_interval_dates[0],
                end=best_interval_dates[1],
            )

    write_json(points, out_path)


def merge_points(json_dir, out_json):
    points = {}
    for f in sorted(os.listdir(json_dir)):
        if not f.endswith('.json'): continue
        dd = os.path.splitext(f)[0]
        logger.info(f)
        points[dd] = read_json(os.path.join(json_dir, f))
    write_json(points, out_json)


def split_rev_json(json_path, out_dir):
    for p in read_json(json_path):
        id = p['id'].replace('/', '__')
        write_json(p, os.path.join(out_dir, id + '.json'))


def add_names(name_jsons, json_dir, out_dir):
    os.makedirs(out_dir, exist_ok=True)
    names = {}
    for json_path in name_jsons:
        d = read_json(json_path)['features']
        for p in d:
            # print(p)
            names[p['id']] = p['properties'].get('name')

    for f in os.listdir(json_dir):
        if not f.endswith('.json'): continue
        d = read_json(os.path.join(json_dir, f))
        for p in d:
            p['name'] = names.get(p['id'])
        write_json(d, os.path.join(out_dir, f))


def add_names_for_rev(name_jsons, json_dir, out_dir):
    os.makedirs(out_dir, exist_ok=True)
    names = {}
    for json_path in name_jsons:
        d = read_json(json_path)['features']
        for p in d:
            names[p['id']] = p['properties'].get('name')

    for f in os.listdir(json_dir):
        if not f.endswith('.json'): continue
        print(f)
        d = read_json(os.path.join(json_dir,f))
        d['name'] = names.get(d['id'])
        write_json(d, os.path.join(out_dir, f))


if __name__ == '__main__':
    pass
    # process_points('../data/points/points.json', date(2017, 1, 1), date(2017, 12, 31), '../data/noaa', '../data/points-noaa-v0')
    # reverse_point_index('../data/sample-date-points', dump_json='../data/sample-date-points.rev.json')
    # reverse_point_index('../data/points-noaa-v0', dump_json='../data/points-noaa-v0.rev.json')

    # add_rev_index_best_intervals('../data/points-noaa-v0.rev.json', '../data/points-noaa-v0.rev.best.json', 0.4)
    # add_rev_index_best_intervals('../data/points-noaa-v0.rev.json', '../data/points-noaa-v0.rev.best.min.json', 0.4, minimal=True)
    # merge_points('../data/points-noaa-v0', '../data/points-noaa-v0.json.gz')
    # merge_points('../data/points-noaa-v0', '../data/points-noaa-v0.json')
    # split_rev_json('../data/points-noaa-v0.rev.json', '../data/points-noaa-v0.rev')

    # add_names(['../data/points_raw/hiking.json', '../data/points_raw/hiking.json'], '../files/points-noaa-v0', '../files/points-noaa-v1')
    # add_names_for_rev(['../data/points_raw/hiking.json', '../data/points_raw/hiking.json'], '../files/points-noaa-v0.rev', '../files/points-noaa-v1.rev')


