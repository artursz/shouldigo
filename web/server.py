from io import BytesIO
from math import pi, cos, sin, log, exp, atan
import sys
import os
from PIL import Image, ImageDraw
import json
import random
import numpy as np
from skimage import draw
from math import sin, cos, sqrt, atan2, radians, ceil
from functools import lru_cache
from flask import Flask, jsonify, request, Response, send_file
from flask_cors import CORS
import utils


import logging

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
logger = logging.getLogger('web')


UI_STATIC_DIR = os.path.realpath(os.path.join(os.path.dirname(__file__), '..', 'ui'))
FILES_DIR = os.path.realpath(os.path.join(os.path.dirname(__file__), '..', 'files'))
POINTS_JSON_PATH = os.getenv('POINTS_JSON_PATH', '../files/points-noaa-v1')
POINTS_REV_JSON_PATH = os.getenv('POINTS_REV_JSON_PATH', '../files/points-noaa-v1.rev')

app = Flask(__name__, static_folder=UI_STATIC_DIR, static_url_path='')
CORS(app)

np.set_printoptions(threshold=np.nan)


def load_points(date_str):
    points = utils.read_json(os.path.join(POINTS_JSON_PATH, date_str + '.json'))

    # random.shuffle(points)
    # points = points[:50] + points[-50:]

    return points


def load_points_by_timeframe(timeframe:int):
    return load_points(scale_to_date_str(timeframe))


def scale_to_date_str(scale):
    month = scale // 3 + 1
    p = scale % 3
    day = 5 if p == 0 else 15 if p == 1 else 25
    return '{:02d}{:02d}'.format(month, day)


SCALED_DATE_STRS_LIST = [scale_to_date_str(e) for e in range(0, 36)]
SCALED_DATE_STRS = set(SCALED_DATE_STRS_LIST)


DEG_TO_RAD = pi / 180
RAD_TO_DEG = 180 / pi
POINT_RADIUS = 100

def calculate_distance():
    # approximate radius of earth in km
    R = 6373.0

    lat1 = radians(52.2296756)
    lon1 = radians(21.0122287)
    lat2 = radians(52.406374)
    lon2 = radians(16.9251681)

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = sin(dlat / 2)**2 + cos(lat1) * cos(lat2) * sin(dlon / 2)**2
    c = 2 * atan2(sqrt(a), sqrt(1 - a))

    distance = R * c


def minmax(a, b, c):
    a = max(a, b)
    a = min(a, c)
    return a


class GoogleProjection:
    def __init__(self, levels=18):
        self.Bc = []
        self.Cc = []
        self.zc = []
        self.Ac = []
        c = 256
        for d in range(0, levels):
            e = c / 2
            self.Bc.append(c / 360.0)
            self.Cc.append(c / (2 * pi))
            self.zc.append((e, e))
            self.Ac.append(c)
            c *= 2

    def fromLLtoPixel(self, ll, zoom):
        d = self.zc[zoom]
        e = round(d[0] + ll[0] * self.Bc[zoom])
        f = minmax(sin(DEG_TO_RAD * ll[1]), -0.9999, 0.9999)
        g = round(d[1] + 0.5 * log((1 + f) / (1 - f)) * -self.Cc[zoom])
        return (e, g)

    def fromPixelToLL(self, px, zoom):
        e = self.zc[zoom]
        f = (px[0] - e[0]) / self.Bc[zoom]
        g = (px[1] - e[1]) / -self.Cc[zoom]
        h = RAD_TO_DEG * (2 * atan(exp(g)) - 0.5 * pi)
        return (f, h)

    def fromZoomToScale(self, zoom, lat):
        return 156543.03392 * cos(lat * pi / 180) / pow(2, zoom)


tileproj = GoogleProjection()


def get_points(l0, l1, timeframe, activity):
    # points = utils.read_json(os.path.join(POINTS_JSON_PATH, scale_to_date_str(timeframe) + '.json'))
    points = load_points_by_timeframe(timeframe)
    points_in_bounds = []
    for point in points:
        if activity in point and l0[0] <= point['x'] <= l1[0] and l0[1] <= point['y'] <= l1[1]:
            points_in_bounds.append({
                'lng': point['x'],
                'lat': point['y'],
                'value': point[activity]['score']
            })

    return points_in_bounds


gradient = np.array(Image.open('gradient.png'))[0]
gradient_r = gradient[:,0]
gradient_g = gradient[:,1]
gradient_b = gradient[:,2]


@lru_cache(maxsize=1000)
def place_points(x, y, z, timeframe, activity):
    tile_color_numerator = np.zeros((256, 256)).flatten()
    tile_color_denominator = np.zeros((256, 256)).flatten()
    tile_intensity = np.zeros((256, 256)).flatten()

    scaled_radius = round(POINT_RADIUS * 1000 / tileproj.fromZoomToScale(z, tileproj.fromPixelToLL((x*256,y*256),z)[1]))

    # Calculate pixel positions of bottom-left & top-right
    p0 = (x * 256 - scaled_radius, (y + 1) * 256 + scaled_radius)
    p1 = ((x + 1) * 256 + scaled_radius, y * 256 - scaled_radius)

    # Convert to LatLong (EPSG:4326)
    l0 = tileproj.fromPixelToLL(p0, z)
    l1 = tileproj.fromPixelToLL(p1, z)

    points_in_bounds = get_points(l0, l1, timeframe, activity)
    if len(points_in_bounds) > 0:

        xx, yy = np.mgrid[:scaled_radius * 2 + 1, :scaled_radius * 2 + 1]
        circle_intensity = 1 - (np.sqrt((xx - scaled_radius) ** 2 + (yy - scaled_radius) ** 2) / scaled_radius)
        circle_intensity[circle_intensity < 0] = 0
        # circle_intensity[circle_intensity > 0] = 1
        # circle_points = circle_intensity > 0

        for idx, point in enumerate(points_in_bounds):

            pixel = tileproj.fromLLtoPixel((point['lng'], point['lat']), z)
            pixel_relative = (pixel[0] - x * 256, pixel[1] - y * 256)

            xx_translated = xx + (pixel_relative[0] - scaled_radius)
            yy_translated = yy + (pixel_relative[1] - scaled_radius)
            mask = (0 <= xx_translated) & (xx_translated <= 255) & (0 <= yy_translated) & (yy_translated <= 255)
            indexes = xx_translated[mask] + 256 * yy_translated[mask]

            point_color_denominator = np.zeros((256, 256)).flatten()
            point_color_denominator[indexes] = circle_intensity[yy[mask], xx[mask]]
            tile_color_denominator += point_color_denominator

            point_color_numerator = np.zeros((256, 256)).flatten()
            point_color_numerator[indexes] = circle_intensity[yy[mask], xx[mask]] * point['value']
            tile_color_numerator += point_color_numerator
            # print(tile_intensity[indexes].shape, circle_intensity[yy[mask], xx[mask]].shape, np.maximum(tile_intensity[indexes], circle_intensity[yy[mask], xx[mask]]).shape)
            tile_intensity[indexes] = np.maximum(tile_intensity[indexes], circle_intensity[yy[mask], xx[mask]])
            # print(np.array2string(point_color_denominator.reshape(256, 256), max_line_width=np.inf))

        tile_color = np.divide(tile_color_numerator, tile_color_denominator, out=np.zeros_like(tile_color_numerator),
                               where=tile_color_denominator != 0)

        data = get_image_data(tile_color, tile_intensity, (256, 256))

    else:
        data = np.zeros((256, 256, 4))
    img = Image.fromarray(data, 'RGBA')
    return img


def get_image_data(values, intensity, shape):
    values = (values*(gradient.shape[0]-1)).astype(np.int32)

    info = np\
        .stack((
            np.apply_along_axis(lambda x: gradient_r[x], 0, values),
            np.apply_along_axis(lambda x: gradient_g[x], 0, values),
            np.apply_along_axis(lambda x: gradient_b[x], 0, values),
            (intensity * 110).astype(np.uint8) if intensity is not None else np.full(values.shape[0], 255, np.uint8)
        ), axis=-1)\
        .flatten()\
        .astype(np.uint8)\
        .reshape(shape[0], shape[1], 4)

    return info


def get_tile(x, y, z, timeframe, activity):
    img = place_points(x, y, z, timeframe, activity)
    buffer = BytesIO()
    img.save(buffer, format='PNG')
    buffer.seek(0)
    return Response(buffer, mimetype='image/png')


@app.route("/api/tile", methods=['GET', 'POST'])
def api_tile():
    x = int(request.args.get('x'))
    y = int(request.args.get('y'))
    z = int(request.args.get('z'))
    timeframe = int(request.args.get('timeframe'))
    activity = request.args.get('activity')
    return get_tile(x, y, z, timeframe, activity)


@app.route('/files/<path:path>')
def files_serve(path):
    filename = os.path.join(FILES_DIR, path)
    return send_file(filename)


@app.route('/')
def index():
    filename = os.path.join(UI_STATIC_DIR, 'index.html')
    return send_file(filename)


@app.route('/api/points1/<string:date>')
def api_points1(date):
    return jsonify(load_points(date))


@app.route('/api/points')
def api_points():
    timeframe = int(request.args.get('scale'))
    activity = request.args.get('activity')
    points = load_points_by_timeframe(timeframe)
    points = [p for p in points if activity in p]
    return jsonify(points)


@app.route('/api/point1')
def api_point_history():
    id = request.args.get('id').replace('/', '__')
    return jsonify(utils.read_json(os.path.join(POINTS_REV_JSON_PATH, id + '.json')))


@app.route('/api/point')
def api_point_history_scaled():
    id = request.args.get('id').replace('/', '__')
    data = utils.read_json(os.path.join(POINTS_REV_JSON_PATH, id + '.json'))
    timeline = data[data['activities'][0]]
    scaled_scores = [t for t in timeline if t['date'] in SCALED_DATE_STRS]
    scaled_measurements = [t for t in data['measurements'] if t['date'] in SCALED_DATE_STRS]
    del data[data['activities'][0]]
    del data['measurements']
    data['measurements'] = scaled_measurements
    data['scores'] = scaled_scores
    return jsonify(data)


@app.route('/api/point_bar')
def api_point_history_scaled_bar():
    id = request.args.get('id').replace('/', '__')
    data = utils.read_json(os.path.join(POINTS_REV_JSON_PATH, id + '.json'))
    timeline = data[data['activities'][0]]
    scaled_scores = [t for t in timeline if t['date'] in SCALED_DATE_STRS]
    scaled_measurements = [t for t in data['measurements'] if t['date'] in SCALED_DATE_STRS]
    del data[data['activities'][0]]
    del data['measurements']

    scores = [x['score'] for x in scaled_scores]
    # print(scores)
    pixels_for_segment = ceil(1000/(len(scores)-1))

    color_values = np.zeros(0)
    for i in range(1, len(scores)):
        step = float(scores[i]-scores[i-1])/float(pixels_for_segment)
        if step == 0:
            color_values = np.append(color_values, np.full(pixels_for_segment, scores[i]))
        else:
            color_values = np.append(color_values, np.arange(scores[i - 1], scores[i],
                                                             float(scores[i] - scores[i - 1]) / float(
                                                                 pixels_for_segment)))

    data = get_image_data(color_values, None, (1, -1))

    img = Image.fromarray(data, 'RGBA')
    buffer = BytesIO()
    img.save(buffer, format='PNG')
    buffer.seek(0)
    return Response(buffer, mimetype='image/png')


@app.route('/api/point_bar_aggregated/<activity>')
def get_aggregated_scores(activity):
    timeframe_scores = []
    for date_str in SCALED_DATE_STRS_LIST:
        points = load_points(date_str)
        scores = [p[activity]['score'] for p in points if activity in p]
        timeframe_scores.append(scores)
    timeframe_scores = np.array(timeframe_scores)
    scores = timeframe_scores.mean(axis=1)

    pixels_for_segment = ceil(1000 / (len(scores) - 1))

    color_values = np.zeros(0)
    for i in range(1, len(scores)):
        step = float(scores[i] - scores[i - 1]) / float(pixels_for_segment)
        if step == 0:
            color_values = np.append(color_values, np.full(pixels_for_segment, scores[i]))
        else:
            color_values = np.append(color_values, np.arange(scores[i - 1], scores[i],
                                                             float(scores[i] - scores[i - 1]) / float(
                                                                 pixels_for_segment)))

    data = get_image_data(color_values, None, (1,-1))

    img = Image.fromarray(data, 'RGBA')
    buffer = BytesIO()
    img.save(buffer, format='PNG')
    buffer.seek(0)
    return Response(buffer, mimetype='image/png')


if __name__ == '__main__':
    PORT = os.getenv('PORT', '5000')
    HOST = os.getenv('HOST', '0.0.0.0')
    print('RUN API %r %r' % (HOST, PORT))
    app.run(host=HOST, port=int(PORT), debug=True, threaded=False)
