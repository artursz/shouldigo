import gzip
import json
import os

def read_json(fpath):
    if fpath.endswith('.gz'):
        # try:
        #     with gzip.open(fpath, "rt", encoding="utf-8") as f:
        #         return json.load(f)
        # except Exception as e:
        #     print(e)
        with gzip.GzipFile(fpath, 'rb') as f:
            json.loads(f.read(), encoding="utf-8")
    else:
        with open(fpath) as f:
             return json.load(f)


def write_json(data, fpath):
    if fpath:
        if fpath.endswith('.gz'):
            with gzip.open(fpath, 'wb') as f:
                f.write(json.dumps(data, indent=2).encode('utf-8'))

        os.makedirs(os.path.dirname(fpath), exist_ok=True)
        with open(fpath, 'w') as f:
             f.write(json.dumps(data, indent=2))