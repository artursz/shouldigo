#!/usr/bin/env bash

if [ $# -eq 0 ]; then
    echo "Build/push backend"
    echo "Tag (default=latest):"
    read TAG
    TAG=${TAG:-latest}
    echo  "TAG='$TAG'"
    docker build -t registry.gitlab.com/artursz/shouldigo:$TAG . && docker push registry.gitlab.com/artursz/shouldigo:$TAG
fi
