FROM python:3.6.6

WORKDIR /app/web

COPY requirements.txt /app/

RUN pip install --upgrade pip && pip install -r /app/requirements.txt

COPY files/sample.json /app/files/sample.json

COPY web /app/web/

COPY ui /app/ui/

ENV PYTHONPATH /app/web
ENV LANG C.UTF-8
ENV PYTHONUNBUFFERED=1
ENV PORT=80

CMD gunicorn server:app -w ${GUNICORN_WORKERS:=1} -b ${HOST:=0.0.0.0}:${PORT} ${GUNICORN_RELOAD:=""} --access-logfile=- --log-level ${GUNICORN_LOG_LEVEL:=info}
